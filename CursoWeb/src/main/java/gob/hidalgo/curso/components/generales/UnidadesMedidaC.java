package gob.hidalgo.curso.components.generales;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.UnidadMedidaEO;
import gob.hidalgo.curso.utils.Modelo;


@Component("UnidadesMedidaC")
public class UnidadesMedidaC {

	@Autowired
	private SqlSession sqlSession; /* declara instancia de base de datos.*/

	@Autowired
	private MensajesC mensajesC;
	
	public Modelo<UnidadMedidaEO> modelo(){
		List<UnidadMedidaEO> listado = sqlSession.selectList("generales.unidadesMedida.listado");
		return new Modelo<UnidadMedidaEO>(listado);
	}
	
	public UnidadMedidaEO nuevo() {
		return new UnidadMedidaEO();
	}
	
	public boolean guardar (UnidadMedidaEO unidad) {
		unidad.setSiglas(unidad.getSiglas().toUpperCase());
		if (unidad.getId()==null) {
			sqlSession.insert("generales.unidadesMedida.insertar", unidad);
			mensajesC.mensajeInfo("Se insert� correctamente");
		}else {
			sqlSession.update("generales.unidadesMedida.actualizar", unidad);
			mensajesC.mensajeInfo("Se actualiz� correctamente");
		}
		return true;
	}
	
	public boolean eliminar (UnidadMedidaEO unidad) {
		sqlSession.delete("generales.unidadesMedida.eliminar", unidad);
		mensajesC.mensajeInfo("Se elimin� correctamente");
		return true;
	}
}
